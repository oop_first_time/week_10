package com.tanitha.week10;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Rectangle rec = new Rectangle(5, 3);
        System.out.println(rec.toString());
        System.out.println(rec.calArea());; //สืบทอดมาจากShape
        System.out.println(rec.calPerimeter()); //สืบทอดมาจากShape

        
        Rectangle rec2 = new Rectangle(2, 2);
        System.out.println(rec2.toString());
        System.out.println(rec2.calArea());; 
        System.out.println(rec2.calPerimeter());
        System.out.println("--------------------------------"); 
        System.err.println();
        

        Circle circle1 = new Circle(2);
        System.out.println(circle1);
        System.out.printf("%s area: %.3f \n", circle1.getName() ,circle1.calArea()); 
        System.out.printf("%s perimeter: %.3f \n", circle1.getName() ,circle1.calPerimeter()); 

        Circle circle2 = new Circle(3);
        System.out.println(circle2);
        System.out.printf("%s area: %.3f \n", circle2.getName() ,circle2.calArea()); 
        System.out.printf("%s area: %.3f \n", circle2.getName() ,circle2.calPerimeter());
        System.out.println("--------------------------------"); 
        System.err.println(); 

        Triangle tri1 = new Triangle(2, 2, 2);
        System.out.println(tri1);
        System.out.printf("%s area: %.3f \n", tri1.getName() ,tri1.calArea()); 
        System.out.printf("%s perimeter: %.3f \n", tri1.getName() ,tri1.calPerimeter()); 
    }
}
