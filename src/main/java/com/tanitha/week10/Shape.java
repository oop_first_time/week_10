package com.tanitha.week10;

public abstract class Shape {
    private String name;
    
    public Shape(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }

    public abstract double calArea(); //abstractนึกไม่ออกคำนวณไม่ได้

    public abstract double calPerimeter();
}
